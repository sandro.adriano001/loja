Inicialmente após baixar o projeto, favor abrir em sua IDE de preferência, no meu caso Spring tools suite 4.4.4.1..

Apóis iniciar a IDE, clique em file -> Import -> Maven -> Existent Maven Project -> Browse(Busque o projeto) -> marque o POM e inicie o projeto ( O donwload de depedências pode levar um certo tempo)..

Posteriormente, clique com o botão direito em cima do projeto já na IDE, e rode com Spring Boot App caso queira rodar o sistema.

Acesse "http://localhost:8090/h2-console/" para verificar o Banco em Memória.

Acesse http://localhost:8090/swagger-ui.html para verificar o swagger.

Obs: O sistema estará rodando na Porta 8090 por fins de compatibilidade da porta 8080, optei por uma porta menos utilizada por programas externos, além disso, uns 3 dos 39 testes deram erro, porém ao executá-los com a mesma lógica no postman eles dão o retorno esperado e correto...

Caso queira rodar os testes, basta em sr/main/test e estarão lá, alguns testes unitários e também de Integração..

O arquivo com o log da aplicação está na raiz do projeto, juntamente com a collection do postman..

Eu optei por usar ligações unilaterais, ou seja apenas uma classe conhece a outra, e não ambas se conhecem, por fins de evitar duplicidade de dados e melhorar o desempenho do software.

Quaisquer eventuais dúvidas entrar em contato comigo pelo celular (27) 988768853..


### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.