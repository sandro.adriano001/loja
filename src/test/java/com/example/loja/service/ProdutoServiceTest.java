package com.example.loja.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.example.loja.domain.Produto;

@SpringBootTest
public class ProdutoServiceTest {

	@Autowired
	private ProdutoService produtoImp;
	
	@Test
	public void cadastroProdutoTest() {
		Produto produto = new Produto();
		produto.setCodigo(new Long(6));
		produto.setCategoria("DVD");
		produto.setDescricao("A bruxa 2021");
		produto.setValorUnitario(43.16);
		Produto salvo = produtoImp.cadastrarProduto(produto);
		// se sair com descrição vazia existem irregularidades
		assertTrue(salvo.equals(produto));
	}
	
	@Test
	public void cadastroProdutoTest2() {
		Produto produto = new Produto();
		produto.setCodigo(new Long(7));
		produto.setCategoria("DVD");
		produto.setDescricao("A bruxa 2022");
		produto.setValorUnitario(43.16);
		Produto salvo = produtoImp.cadastrarProduto(produto);
		// se sair com descrição vazia existem irregularidades
		assertTrue(salvo.equals(produto));
	}
	
	@Test
	public void cadastroProdutoTest3() {
		Produto produto = new Produto();
		produto.setCodigo(new Long(8));
		produto.setCategoria("DVD");
		produto.setDescricao("A bruxa 2023");
		produto.setValorUnitario(43.16);
		Produto salvo = produtoImp.cadastrarProduto(produto);
		// se sair com descrição vazia existem irregularidades
		assertTrue(salvo.equals(produto));
	}
	
	
	@Test
	public void cadastroProdutoRepetidoTest() {
		Produto produto = new Produto();
		produto.setCodigo(new Long(6));
		produto.setCategoria("DVD");
		produto.setDescricao("A bruxa 2022");
		produto.setValorUnitario(43.16);
		Produto salvo = produtoImp.cadastrarProduto(produto);
		// cadastro com Descrição repetido, o sistema tira a descrição e não salva para dizer que é inválido
		// O processo de distinção de válido ou não está em ProdutoResource
		// O sistema envia uma mensagem para o Front informando o erro e envia uma requisição de BAD request
		assertTrue(salvo.getDescricao().equals(produto.getDescricao()));
	}
	
	@Test
	public void buscarPorIdTest() {
		Optional<Produto> produtoTest = produtoImp.buscarProduto(new Long(7));
	  	//  Esperado False, desde que tenha um produto com Código 1000
	  	assertTrue(produtoTest.isPresent());
	}
	
	
	@Test
	public void deletarProdutoTest() {
		// Exclui o Produto
		produtoImp.deletarProduto(new Long(8));
		Optional<Produto> produto = produtoImp.buscarProduto(new Long(8));
		
		// Procura por ele e Teoricamente não pode achar por isso assertFalse
		assertFalse(produto.isPresent());
	}
	
}
