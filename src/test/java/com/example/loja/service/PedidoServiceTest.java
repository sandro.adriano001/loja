package com.example.loja.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.example.loja.domain.Pedido;
import com.example.loja.domain.Produto;
import com.example.loja.domain.ProdutoPedido;

@SpringBootTest
public class PedidoServiceTest {

	@Autowired
	private PedidoService pedidoImp;
	
	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private ProdutoService produtoService;
	
	@Test
	public void cadastroPedidoTest() {
		Pedido pedidoTest = new Pedido();
		pedidoTest.setNumeroPedido(new Long(1001));
		pedidoTest.setCliente(clienteService.buscarCliente("12560585640").get());
		pedidoTest.setDataPedido(new Date(2020-01-20));
		pedidoTest.setValorFrete(new Double(0));
		pedidoTest.setValorTotal(new Double(0));
		
		Produto produtoT = new Produto();
			produtoT.setCodigo(new Long(1005));
			produtoT.setCategoria("Blueray");
			produtoT.setDescricao("Titanic2020");
			produtoT.setValorUnitario(43.12);
		produtoService.cadastrarProduto(produtoT);	
		
		ProdutoPedido item = new ProdutoPedido();
		item.setCodigo(new Long(10));
		item.setProduto(produtoT);
		item.setQuantidade(5);
		item.setValorTotal(0);
		
		List<ProdutoPedido> lista = new ArrayList<ProdutoPedido>();
		lista.add(item);
		
		pedidoTest.setItens(lista);
		Pedido salvo = pedidoImp.validarPedido(pedidoTest);
		pedidoImp.cadastrarPedido(salvo);
		assertTrue(salvo.getNumeroPedido().equals(new Long(1001)));
	}
	
	@Test
	public void cadastroPedidoTest2() {
		Pedido pedidoTest = new Pedido();
		pedidoTest.setNumeroPedido(new Long(1002));
		pedidoTest.setCliente(clienteService.buscarCliente("12560585640").get());
		pedidoTest.setDataPedido(new Date(2020-01-20));
		pedidoTest.setValorFrete(new Double(0));
		pedidoTest.setValorTotal(new Double(0));
		
		Produto produtoT = new Produto();
			produtoT.setCodigo(new Long(1004));
			produtoT.setCategoria("Blueray");
			produtoT.setDescricao("Titanic2021");
			produtoT.setValorUnitario(43.12);
		produtoService.cadastrarProduto(produtoT);	
		
		ProdutoPedido item = new ProdutoPedido();
		item.setCodigo(new Long(11));
		item.setProduto(produtoT);
		item.setQuantidade(5);
		item.setValorTotal(0);
		
		List<ProdutoPedido> lista = new ArrayList<ProdutoPedido>();
		lista.add(item);
		
		pedidoTest.setItens(lista);
		Pedido salvo = pedidoImp.validarPedido(pedidoTest);
		pedidoImp.cadastrarPedido(salvo);
		assertTrue(salvo.getNumeroPedido().equals(new Long(1002)));
	}
	
	@Test
	public void buscarPorIdErradoTest() {
		Optional<Pedido> pedidoTest = pedidoImp.buscarPedido(new Long(1013));
	  	//  Esperado False, desde que tenha um pedido com Código 100
		//  Caso Dê erro significa que existe um pedido com esse código
	  	assertFalse(pedidoTest.isPresent());
	}
	
}
