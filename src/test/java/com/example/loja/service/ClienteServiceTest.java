package com.example.loja.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.example.loja.domain.Cliente;

@SpringBootTest
public class ClienteServiceTest {
	
	
	@Autowired
	private ClienteService clienteImp;
	
	@Test
	public void cadastroClienteTest() {
		Cliente auxiliar = new Cliente();
		auxiliar.setCpf("12560585647");
		auxiliar.setNome("sandro901");
		auxiliar.setCep("35029123");
		auxiliar.setEndereco("rua joão");
		auxiliar.setTelefone("27988768812");
		auxiliar.setUf("MG");
		auxiliar.setCidade("Governador");
		auxiliar.setBairro("sao paulo");
		Cliente salvo = clienteImp.cadastrarCliente(auxiliar);
		assertTrue(salvo.getNome().equalsIgnoreCase("sandro901"));
	}
	
	@Test
	public void cadastroClienteTest2Sucesso() {
		Cliente auxiliar = new Cliente();
		auxiliar.setCpf("12560585640");
		auxiliar.setNome("sandro900");
		auxiliar.setCep("35029123");
		auxiliar.setEndereco("rua joão");
		auxiliar.setTelefone("27988768812");
		auxiliar.setUf("MG");
		auxiliar.setCidade("Governador");
		auxiliar.setBairro("sao paulo");
		Cliente salvo = clienteImp.cadastrarCliente(auxiliar);
		assertTrue(salvo.getNome().equalsIgnoreCase("sandro900"));
	}
	
	@Test
	public void cadastroRepetidoTest() {
		Cliente auxiliar = new Cliente();
		auxiliar.setCpf("12560585647");
		auxiliar.setNome("sandro901");
		auxiliar.setCep("35029123");
		auxiliar.setEndereco("rua joão");
		auxiliar.setTelefone("27988768812");
		auxiliar.setUf("MG");
		auxiliar.setCidade("Governador");
		auxiliar.setBairro("sao paulo");
		Cliente salvo = clienteImp.cadastrarCliente(auxiliar);
		// cadastro com Nome repetido, o sistema tira o nome do cliente e não salva para dizer que é inválido
		// O processo de distinção de válido ou não está em ClienteResource
		// O sistema envia uma mensagem para o Front informando o erro e envia uma requisição de BAD request
		assertEquals("", salvo.getNome());
	}
	
	@Test
	public void buscarPorIdSucessTest() {
		Optional<Cliente> clienteTest = clienteImp.buscarCliente("12560585640");
	//  Esperado true	
		assertTrue(clienteTest.get().getNome().equals("sandro900"));
	//	Esperado false
		assertFalse(clienteTest.get().getCpf().equals("35029123"));
	}
	
	
	@Test
	public void excludeClienteTest() {
		// Exclui o Cliente
		 clienteImp.deletarCliente("12560585647");
		 // Procura por ele e Teoricamente não pode achar
		 assertFalse(clienteImp.buscarCliente("12560585647").isPresent());
	}
	
}
