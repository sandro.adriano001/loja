package com.example.loja.repository.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import com.example.loja.domain.Cliente;
import com.example.loja.domain.Pedido;
import com.example.loja.domain.Produto;
import com.example.loja.domain.ProdutoPedido;
import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PedidoRepositoryTest {

	@Autowired
	public WebApplicationContext context;
	
	@Autowired
	private MockMvc mvc;
	
	 @Autowired
	 private ObjectMapper objectMapper;
	
	public Pedido pedido;
	
	
	@Test
	public void testaRequisiçãoCadastroSucesso() throws Exception { 
		Cliente clienteT = new Cliente();
		clienteT.setCpf("12560874510");
		
		Produto produtoT = new Produto();
		produtoT.setCodigo(new Long(1002));
		
		List<ProdutoPedido> lista = new ArrayList<ProdutoPedido>();
		
		ProdutoPedido item = new ProdutoPedido();
		item.setCodigo(new Long(100));
		item.setProduto(produtoT);
		item.setQuantidade(2);
		item.setValorTotal(0);
		lista.add(item);
		
		Pedido pedidoT = new Pedido();
		pedidoT.setNumeroPedido(new Long(1018));
		pedidoT.setCliente(clienteT);
		pedidoT.setDataPedido(new Date(20-01-2020));
		pedidoT.setValorFrete(0);
		pedidoT.setValorTotal(0);
		pedidoT.setItens(lista);
		
		this.mvc.perform(post("/pedido")
		.contentType("application/json")
		.content(objectMapper.writeValueAsString(pedidoT)))
		.andExpect(status().isOk());
	}

	
	@Test
	public void testaRequisiçãoBuscaIDSucesso() throws Exception { 
		this.mvc.perform(get("/pedido/1018")
		.contentType("application/json")
		.contentType(objectMapper.writeValueAsString(pedido)))
		.andExpect(status().isOk());
	}
	
	@Test
	public void testaRequisiçãoBuscaIDFalha() throws Exception { 
		this.mvc.perform(get("/pedido/101212")
		.contentType("application/json")
		.contentType(objectMapper.writeValueAsString(pedido)))
		.andExpect(status().isBadRequest());
	}
	
	@Test
	public void testaRequisiçãoDeleteSucesso() throws Exception { 
		this.mvc.perform(delete("/pedido/1018")
		.contentType("application/json")
		.contentType(objectMapper.writeValueAsString(pedido)))
		.andExpect(status().is(200));
	}
	
	@Test
	public void testaRequisiçãoDeleteFalha() throws Exception { 
		this.mvc.perform(delete("/pedido/1018")
		.contentType("application/json")
		.contentType(objectMapper.writeValueAsString(pedido)))
		.andExpect(status().isNotFound());
	}
	 
}
