package com.example.loja.repository.test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import com.example.loja.domain.Produto;
import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProdutoRepositoryTest {

	@Autowired
	public WebApplicationContext context;
	
	@Autowired
	private MockMvc mvc;
	
	 @Autowired
	 private ObjectMapper objectMapper;
	 
	 public Produto produto; 
	 
	    @Test
		public void testaRequisiçãoCadastroSucesso() throws Exception { 
			Produto produtoT = new Produto();
			produtoT.setCodigo(new Long(1001));
			produtoT.setCategoria("Blueray");
			produtoT.setDescricao("Titanic");
			produtoT.setValorUnitario(43.12);
			this.mvc.perform(post("/produto")
			.contentType("application/json")
			.content(objectMapper.writeValueAsString(produtoT)))
			.andExpect(status().isOk())
			.andExpect(jsonPath("descricao", equalTo("Titanic")));
		}
	    
	    @Test
	  		public void testaRequisiçãoCadastroSucesso2() throws Exception { 
	  			Produto produtoT = new Produto();
	  			produtoT.setCodigo(new Long(1002));
	  			produtoT.setCategoria("Blueray");
	  			produtoT.setDescricao("Titanic2");
	  			produtoT.setValorUnitario(43.12);
	  			this.mvc.perform(post("/produto")
	  			.contentType("application/json")
	  			.content(objectMapper.writeValueAsString(produtoT)))
	  			.andExpect(status().isOk())
	  			.andExpect(jsonPath("descricao", equalTo("Titanic2")));
	  		}
	 
		@Test
		public void testaRequisiçãoBuscaIDSucesso() throws Exception { 
			this.mvc.perform(get("/produto/1002")
			.contentType("application/json")
			.content(objectMapper.writeValueAsString(produto)))
			.andExpect(status().isOk())
			.andExpect(jsonPath("descricao", equalTo("Titanic2")));
		}
			
		@Test
		public void testaRequisiçãoBuscaIDFalha() throws Exception { 
			this.mvc.perform(get("/produto/-1")
			.contentType("application/json")
			.contentType(objectMapper.writeValueAsString(produto)))
			.andExpect(status().isBadRequest());
		}
		
		@Test
		public void testaRequisiçãoCadastroFalha() throws Exception { 
			Produto produtoT = new Produto();
			produtoT.setCodigo(new Long(1002));
			produtoT.setCategoria("Blueray");
			produtoT.setDescricao("Titanic2");
			produtoT.setValorUnitario(43.12);
			this.mvc.perform(post("/produto")
			.contentType("application/json")
			.content(objectMapper.writeValueAsString(produtoT)))
			.andExpect(status().isBadRequest());
		}
	
	@Test
	public void testaRequisiçãoDeleteSucesso() throws Exception { 
		this.mvc.perform(delete("/produto/1001")
		.contentType("application/json")
		.contentType(objectMapper.writeValueAsString(produto)))
		.andExpect(status().is(200));
	}
	
	@Test
	public void testaRequisiçãoDeleteFalha() throws Exception { 
		this.mvc.perform(delete("/produto/1001")
		.contentType("application/json")
		.contentType(objectMapper.writeValueAsString(produto)))
		.andExpect(status().isNotFound());
	}
	 
}
