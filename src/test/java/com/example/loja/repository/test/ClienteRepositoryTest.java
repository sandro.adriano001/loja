package com.example.loja.repository.test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import com.example.loja.domain.Cliente;
import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ClienteRepositoryTest {

	@Autowired
	public WebApplicationContext context;
	
	@Autowired
	private MockMvc mvc;
	
	 @Autowired
	 private ObjectMapper objectMapper;
	
	public Cliente cliente;
	
	
	@Test
	public void testaRequisiçãoCadastroSucesso() throws Exception { 
		Cliente clienteT = new Cliente();
		clienteT.setCpf("12560874509");
		clienteT.setCep("35020129");
		clienteT.setCidade("Ouro Preto");
		clienteT.setBairro("jardim");
		clienteT.setEndereco("rua josé lopes");
		clienteT.setNome("Joaquin");
		clienteT.setTelefone("33988738823");
		this.mvc.perform(post("/cliente")
		.contentType("application/json")
		.content(objectMapper.writeValueAsString(clienteT)))
		.andExpect(status().isOk())
		.andExpect(jsonPath("nome", equalTo("Joaquin")));
	}
	
	@Test
	public void testaRequisiçãoCadastroSucesso2() throws Exception { 
		Cliente clienteT = new Cliente();
		clienteT.setCpf("12560874510");
		clienteT.setCep("35020129");
		clienteT.setCidade("Ouro Preto");
		clienteT.setBairro("jardim");
		clienteT.setEndereco("rua josé lopes");
		clienteT.setNome("Joaquin Santos");
		clienteT.setTelefone("33988738823");
		this.mvc.perform(post("/cliente")
		.contentType("application/json")
		.content(objectMapper.writeValueAsString(clienteT)))
		.andExpect(status().isOk())
		.andExpect(jsonPath("nome", equalTo("Joaquin Santos")));
	}
	
	@Test
	public void testaRequisiçãoCadastroFalha() throws Exception { 
		Cliente clienteT = new Cliente();
		clienteT.setCpf("12560874510");
		clienteT.setCep("35020129");
		clienteT.setCidade("Ouro Preto");
		clienteT.setBairro("jardim");
		clienteT.setEndereco("rua josé lopes");
		clienteT.setNome("Joaquin Santos");
		clienteT.setTelefone("33988738823");
		this.mvc.perform(post("/cliente")
		.contentType("application/json")
		.content(objectMapper.writeValueAsString(clienteT)))
		.andExpect(status().isBadRequest());
	}
	
	@Test
	public void testaRequisiçãoIDSucesso() throws Exception { 
		this.mvc.perform(get("/cliente/12560874510")
		.contentType("application/json")
		.content(objectMapper.writeValueAsString(cliente)))
		.andExpect(status().isOk())
		.andExpect(jsonPath("nome", equalTo("Joaquin Santos")));
	}
	
	@Test
	public void testaRequisiçãoIDFalha() throws Exception { 
		this.mvc.perform(get("/cliente/125605856406")
		.contentType("application/json")
		.contentType(objectMapper.writeValueAsString(cliente)))
		.andExpect(status().isBadRequest());
	}
	
	@Test
	public void testaRequisiçãoDeleteSucesso() throws Exception { 
		this.mvc.perform(delete("/cliente/12560874509")
		.contentType("application/json")
		.contentType(objectMapper.writeValueAsString(cliente)))
		.andExpect(status().is(200));
	}
	
	@Test
	public void testaRequisiçãoDeleteFalha() throws Exception { 
		this.mvc.perform(delete("/cliente/12560874509")
		.contentType("application/json")
		.contentType(objectMapper.writeValueAsString(cliente)))
		.andExpect(status().isNotFound());
	}
	
}
