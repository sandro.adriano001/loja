package com.example.loja;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LojaXApplication {

	public static void main(String[] args) {
		SpringApplication.run(LojaXApplication.class, args);
	}

}
