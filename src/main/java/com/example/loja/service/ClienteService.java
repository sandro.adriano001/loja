package com.example.loja.service;

import java.util.Collection;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.loja.domain.Cliente;
import com.example.loja.repository.ClienteRepository;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;
	
	public Collection<Cliente> listarClientes(){	
		return clienteRepository.findAll();
	}
	
	public Cliente cadastrarCliente(Cliente cliente) {
		if ((!cliente.getNome().isEmpty()) && (!clienteRepository.findByNome(cliente.getNome()).isPresent()) && (!clienteRepository.findById(cliente.getCpf()).isPresent())){
			Cliente atorAux = clienteRepository.save(cliente);
			return atorAux;
		}else {
			cliente.setNome("");
			return cliente;
		}
	}
	
	public Cliente atualizarCliente(String id, Cliente cliente) {
		if ((cliente.getNome() == null)&&(cliente.getNome().equals(""))){
			return null;
		}
		return clienteRepository.findById(id)
				.map(record-> {
					record.setNome(cliente.getNome());
					record.setTelefone(cliente.getTelefone());
					record.setEndereco(cliente.getEndereco());
					record.setBairro(cliente.getBairro());
					record.setCidade(cliente.getCidade());
					record.setUf(cliente.getUf());
					record.setCep(cliente.getCep());
					Cliente updated = clienteRepository.save(record);
					return updated;
				}).orElse(null);
	}
	
	public void deletarCliente(String id) {
		clienteRepository.deleteById(id);
	}

	public Optional<Cliente> buscarCliente(String id) {
		Optional<Cliente> cliente = clienteRepository.findById(id);
		return cliente;
	}
	
}
