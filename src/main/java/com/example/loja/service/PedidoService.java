package com.example.loja.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.loja.domain.Pedido;
import com.example.loja.domain.Produto;
import com.example.loja.repository.ClienteRepository;
import com.example.loja.repository.PedidoRepository;
import com.example.loja.repository.ProdutoPedidoRepository;
import com.example.loja.repository.ProdutoRepository;

@Service
public class PedidoService {

	@Autowired
	private PedidoRepository pedidoRepository;
	
	@Autowired
	private ProdutoPedidoRepository produtoPedidoRepository;
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	
	public Collection<Pedido> listarPedidos(){	
		return pedidoRepository.findAll();
	}
	
	
	public Pedido cadastrarPedido(Pedido pedido) {
			Pedido pedidoAux = pedidoRepository.save(pedido);
			return pedidoAux;
	}
	
	
	// Faz a busca dos fretes de acordo com cada estado
	public Pedido calcularValorFrete(Pedido pedido) {
		
		String cidade = clienteRepository.findById(pedido.getCliente().getCpf()).get().getUf();
		switch (cidade) {
			case "AC":
				pedido.setValorFrete(62.75);
				return pedido;
			case "AL":
				pedido.setValorFrete(5.21);
				return pedido;
			case "AM":
				pedido.setValorFrete(53.74);
				return pedido;
			case "AP":
				pedido.setValorFrete(44.12);
				return pedido;
			case "BA":
				pedido.setValorFrete(24.36);
				return pedido;
			case "CE":
				pedido.setValorFrete(33.52);
				return pedido;
			case "DF":
				pedido.setValorFrete(19.54);
				return pedido;
			case "ES":
				pedido.setValorFrete(47.96);
				return pedido;
			case "GO":
				pedido.setValorFrete(19.89);
				return pedido;
			case "MA":
				pedido.setValorFrete(16.57);
				return pedido;
			case "MG":
				pedido.setValorFrete(0);
				return pedido;
			case "MS":
				pedido.setValorFrete(8.95);
				return pedido;
			case "MT":
				pedido.setValorFrete(14.59);
				return pedido;
			case "PA":
				pedido.setValorFrete(52.41);
				return pedido;
			case "PB":
				pedido.setValorFrete(19.84);
				return pedido;
			case "PE":
				pedido.setValorFrete(42.76);
				return pedido;
			case "PI":
				pedido.setValorFrete(29.65);
				return pedido;
			case "PR":
				pedido.setValorFrete(24.24);
				return pedido;
			case "RJ":
				pedido.setValorFrete(24.65);
				return pedido;
			case "RN":
				pedido.setValorFrete(56.77);
				return pedido;
			case "RO":
				pedido.setValorFrete(68.48);
				return pedido;
			case "RR":
				pedido.setValorFrete(57.78);
				return pedido;
			case "RS":
				pedido.setValorFrete(22.57);
				return pedido;
			case "SC":
				pedido.setValorFrete(16.45);
				return pedido;
			case "SE":
				pedido.setValorFrete(18.98);
				return pedido;
			case "SP":
				pedido.setValorFrete(2);
				return pedido;
			case "TO":
				pedido.setValorFrete(8.59);
				return pedido;
				
			default: return pedido;
		
		}
	}
	
	// Atualiza os valores
	public Pedido atualizarPedido(Long id, Pedido pedido) {

		return pedidoRepository.findById(id)
				.map(record-> {
					record.setCliente(pedido.getCliente());
					record.setDataPedido(pedido.getDataPedido());
					record.setValorFrete(pedido.getValorFrete());
					record.setValorTotal(pedido.getValorTotal());
					record.setItens(pedido.getItens());
					Pedido updated = pedidoRepository.save(record);
					return updated;
				}).orElse(null);
	}
	
	public void deletarPedido(Long id) {
		pedidoRepository.deleteById(id);
	}
	
	public Optional<Pedido> buscarPedido(Long id) {
		Optional<Pedido> pedido = pedidoRepository.findById(id);
		return pedido;
	}
	
	public Pedido salvarPedido(Pedido pedido) {
		return pedidoRepository.save(pedido);
	}
	
	public List<Pedido> salvarTodosPedidos(List<Pedido> pedidos) {
		return pedidoRepository.saveAll(pedidos);
	}
	 
	// Verifica se existe outro pedido com o mesmo código
	public Pedido validarPedido(Pedido pedido) {
		Optional<Pedido> aux = pedidoRepository.findById(pedido.getNumeroPedido());
		if(aux.isPresent()) return null;
		else return pedido;
	}
	
	// Soma os valores dos itens com as quantidades, também adiciona o frete ao valor total
	public Pedido itens(Pedido pedido){
		for(int i=0; i< pedido.getItens().size(); i++) {
			Optional<Produto> produtoAux = produtoRepository.findById(pedido.getItens().get(i).getProduto().getCodigo());
			if(!produtoAux.isPresent()) {
				pedido.setItens(null);
				return pedido;
			}
			double valorUnitario = produtoAux.get().getValorUnitario();
			pedido.setValorTotal(pedido.getValorTotal() + (valorUnitario)*(pedido.getItens().get(i).getQuantidade()));
			pedido.getItens().get(i).setValorTotal(valorUnitario * pedido.getItens().get(i).getQuantidade());
			if(pedido.getItens().get(i).getQuantidade() <= 0) {
				pedido.setItens(null);
				return pedido;
			}
		}
		produtoPedidoRepository.saveAll(pedido.getItens());
		return pedido;
	}
	
}
