package com.example.loja.service;

import java.util.Collection;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.loja.domain.Produto;
import com.example.loja.repository.ProdutoRepository;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository produtoRepository;
	
	public Collection<Produto> listarProdutos(){	
		return produtoRepository.findAll();
	}
	
	public Produto cadastrarProduto(Produto produto) {

			if ((!produto.getDescricao().isEmpty()) && (!produtoRepository.findById(produto.getCodigo()).isPresent()) && (!produtoRepository.findByDescricao(produto.getDescricao()).isPresent())){
				Produto produtoAux = produtoRepository.save(produto);
				return produtoAux;
			}else {
				produto.setDescricao("");
				return produto;
			}
	}
	
	public Produto atualizarProduto(Long id, Produto produto) {
		if ((produto.getDescricao() == null)&&(produto.getDescricao().equals(""))){
			return null;
		}
		
		return produtoRepository.findById(id)
				.map(record-> {
					record.setDescricao(produto.getDescricao());
					record.setCategoria(produto.getCategoria());
					record.setValorUnitario(produto.getValorUnitario());
					Produto updated = produtoRepository.save(record);
					return updated;
				}).orElse(null);
	}
	
	public void deletarProduto(Long id) {
		produtoRepository.deleteById(id);
	}

	public Optional<Produto> buscarProduto(Long id) {
		Optional<Produto> produto = produtoRepository.findById(id);
		return produto;
	}
	
}
