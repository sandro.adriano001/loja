package com.example.loja.resource;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.Collections;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.any())
        .paths(PathSelectors.any())
        .build()
        .apiInfo(buildApiInfo());
  }

  private ApiInfo buildApiInfo() {
    return new ApiInfo(
      "Loja API",                                                               
      "API para gerenciamento de Loja",    // Descrição
      "1.0",                                                                              // Versão
      "",                                                                                 
      new Contact("Sandro Adriano", "Cel: (27)988768853", "sandro.adriano001@gmail.com"),  // Contato
      "Sandro Develop..",                                                               
      "https://swagger.com",                                                             
      Collections.emptyList()                                                            
    );
  }

}