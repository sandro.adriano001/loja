package com.example.loja.resource;

import java.util.Collection;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.loja.domain.Produto;
import com.example.loja.service.ProdutoService;

@RestController
@RequestMapping("/produto")
public class ProdutoResource {

	@Autowired
	private ProdutoService produtoService;
	
	@GetMapping
	public Collection<Produto> listarProdutos(){
		return produtoService.listarProdutos();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> buscarProduto(@PathVariable Long id) {
		Optional<Produto> produto = produtoService.buscarProduto(id);
		return produto.isPresent() ? ResponseEntity.ok(produto) : ResponseEntity.badRequest().body("Produto Não Encontrado no Sistema!!");
	}
	
	@PostMapping
	public ResponseEntity<?> cadastrarProduto(@RequestBody Produto produto) {
		Produto Aux = produtoService.cadastrarProduto(produto); 
		return  !Aux.getDescricao().equals("") ? ResponseEntity.ok(Aux) : ResponseEntity.badRequest().body("Descrição ou Código do Produto Vazio Ou Já Repetido com um anteriormente Inserido!!");
	}
	
	@PutMapping("/{id}")
	public Produto atualizarProduto(@PathVariable Long id, @RequestBody Produto produto) {
		return produtoService.atualizarProduto(id, produto);
	}
	
	@DeleteMapping("/{id}")
	public void deletarProduto(@PathVariable Long id) {
		produtoService.deletarProduto(id);
	}
	
}
