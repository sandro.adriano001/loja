package com.example.loja.resource;

import java.util.Collection;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.loja.domain.Cliente;
import com.example.loja.service.ClienteService;

@RestController
@RequestMapping("/cliente")
public class ClienteResource {

	@Autowired
	private ClienteService clienteService;
	
	@GetMapping
	public Collection<Cliente> listarClientes(){
		return clienteService.listarClientes();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> buscarCliente(@PathVariable String id) {
		Optional<Cliente> aux =  clienteService.buscarCliente(id);
		return  aux.isPresent() ? ResponseEntity.ok(aux) : ResponseEntity.badRequest().body("Cliente Não Encontrado!");
	}
	
	@PostMapping
	public ResponseEntity<?> cadastrarCliente(@RequestBody Cliente cliente) {
		Cliente Aux = clienteService.cadastrarCliente(cliente);
		return  !Aux.getNome().isEmpty() ? ResponseEntity.ok(Aux) : ResponseEntity.badRequest().body("Nome do Cliente Vazio Ou Já Repetido com um anteriormente Inserido!!");
	}
	
	@PutMapping("/{id}")
	public Cliente atualizarCliente(@PathVariable String id, @RequestBody Cliente cliente) {
		return clienteService.atualizarCliente(id, cliente);
	}
	
	@DeleteMapping("/{id}")
	public void deletarCliente(@PathVariable String id) {
		clienteService.deletarCliente(id);
	}
}
