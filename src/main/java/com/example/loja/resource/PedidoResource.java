package com.example.loja.resource;

import java.util.Collection;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.loja.domain.Pedido;
import com.example.loja.service.PedidoService;


@RestController
@RequestMapping("/pedido")
public class PedidoResource {

	@Autowired
	private PedidoService pedidoService;
	
	@GetMapping
	public Collection<Pedido> listarPedidos(){
		return pedidoService.listarPedidos();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> NotaFiscal(@PathVariable Long id) {
		Optional<Pedido> pedido = pedidoService.buscarPedido(id);
		return pedido.isPresent() ? ResponseEntity.ok(pedido.get()) : ResponseEntity.badRequest().body("Pedido não encontrado no Sistema!!");
	}
	
	@PostMapping
	public ResponseEntity<?> cadastrarPedido(@RequestBody Pedido pedido) {
		pedidoService.calcularValorFrete(pedido);
		pedido.setValorTotal(pedido.getValorFrete());
		pedido = pedidoService.validarPedido(pedido);
		if(pedido == null) return  ResponseEntity.badRequest().body("Código do Pedido repetido!!");
		pedido =  pedidoService.itens(pedido);
		if(pedido.getItens() == null) return ResponseEntity.badRequest().body("Item encontrado de forma Irregular!!");
		Pedido Aux = pedidoService.cadastrarPedido(pedido); 
		return  !Aux.getCliente().getNome().isEmpty() ? ResponseEntity.ok(Aux) : ResponseEntity.badRequest().body("Nome do Cliente Vazio Ou Irregular!!");
	}
	
	@PutMapping("/{id}")
	public Pedido atualizarPedido(@PathVariable Long id, @RequestBody Pedido pedido) {
		return pedidoService.atualizarPedido(id, pedido);
	}
	
	@DeleteMapping("/{id}")
	public void deletarPedido(@PathVariable Long id) {
		pedidoService.deletarPedido(id);
	}
}
