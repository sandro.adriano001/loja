package com.example.loja.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import com.sun.istack.NotNull;
import lombok.Data;

@Entity
@Data
public class Cliente {
	
	@Id
	@Column(length = 11)
	private String cpf;
	
	@NotNull
	private String nome;
	
	@NotNull
	private String endereco;
	
	@Column(length = 9)
	@NotNull
	private String cep;
	
	@NotNull
	private String bairro;
	
	@NotNull
	private String cidade;
	
	@NotNull
	private String uf;
	
	@Column(length = 11)
	private String telefone;
}
