package com.example.loja.domain;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import org.springframework.format.annotation.DateTimeFormat;
import lombok.Data;

@Entity
@Data
public class Pedido {

	@Id
	private Long numeroPedido;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy ")
	private Date dataPedido;
	
	@ManyToOne
	@JoinColumn(name= "id_cliente")
	private Cliente cliente;
	
	@OneToMany(cascade = CascadeType.ALL)
	List<ProdutoPedido> itens;
	
	private double valorTotal;
	
	private double valorFrete;
	
}
