package com.example.loja.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import com.sun.istack.NotNull;
import lombok.Data;

@Entity
@Data
public class Produto {
	
	@Id
	private Long codigo;
	
	@NotNull
	@Column(unique = true)
	private String descricao;
	
	private String categoria;
	
	private double valorUnitario;
	
}
