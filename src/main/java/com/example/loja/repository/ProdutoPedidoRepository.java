package com.example.loja.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.example.loja.domain.ProdutoPedido;

public interface ProdutoPedidoRepository extends JpaRepository<ProdutoPedido, Long>{
	
	List<ProdutoPedido> findByProduto_codigo(String codigo);
}
