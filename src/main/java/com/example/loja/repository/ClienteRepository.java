package com.example.loja.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.example.loja.domain.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, String> {

	Optional<Cliente> findByNome(String nome);
	
}
