package com.example.loja.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.example.loja.domain.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {

	Optional<Produto> findByDescricao(String descricao);
}
