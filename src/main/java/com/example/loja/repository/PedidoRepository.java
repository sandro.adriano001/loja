package com.example.loja.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.loja.domain.Pedido;

public interface PedidoRepository extends JpaRepository<Pedido, Long> {

}
